const fs= require('fs-extra')

const {parse} = require('csv-parse/sync')

function extra_run_conceded_by_each_team_in_year_2016(){

    try{
        
        const csv_match_data =  fs.readFileSync('src/data/matches.csv', 'utf-8')

        const match_data = parse(
            csv_match_data,
            {
                columns: true,
                skip_empty_lines : true
            }
        )

        const csv_delivery_data = fs.readFileSync('src/data/deliveries.csv', 'utf-8')

        const delivery_data = parse(
            csv_delivery_data,
            {
                columns: true,
                skip_empty_lines : true
            }
        )

    
        let by_each_team_extra_run_conceded_in_2016 = {}
    
        for(let index=0; index<delivery_data.length; index++){
    
            let match_id = parseInt(delivery_data[index].match_id);
    
            let season = match_data[match_id-1].season; 
    
            if(season === '2016'){
    
                let bowling_team = delivery_data[index].bowling_team
        
                let extra_run_conceded = delivery_data[index].extra_runs
        
                if(!by_each_team_extra_run_conceded_in_2016[bowling_team]){
                    by_each_team_extra_run_conceded_in_2016[bowling_team] = 0
                }
        
                if(parseInt(extra_run_conceded)){
                    by_each_team_extra_run_conceded_in_2016[bowling_team] += parseInt(extra_run_conceded)
                }
            }
    
        }
        
        return by_each_team_extra_run_conceded_in_2016;
    }
    catch(err){

        throw new Error(err)

    }


}

function dump_in_json_file(){
    
    try{
        
        const result = extra_run_conceded_by_each_team_in_year_2016();
    
        let address = 'src/public/output/extraRunConcededByEachTeamInYear2016.json'
        
        fs.outputJson( address, result, {spaces : 2}).then(() => fs.readJson(address)).then().catch(err => {console.log(err)})

    }catch(err){
        
        console.log(err.stack)

    }

}

dump_in_json_file()