const fs= require('fs-extra')

const {parse} = require('csv-parse/sync')

function highest_number_of_times_one_player_is_dismissed_by_other_player(){

    try{

        const csv_match_data =  fs.readFileSync('src/data/matches.csv', 'utf-8')
        
        const match_data = parse(
            csv_match_data,
            {
                columns: true,
                skip_empty_lines : true
            }
        )
    
        const csv_delivery_data = fs.readFileSync('src/data/deliveries.csv', 'utf-8')
    
        const delivery_data = parse(
            csv_delivery_data,
            {
                columns: true,
                skip_empty_lines : true
            }
        )
        
        let all_bowlers_who_dismissed_a_batsman = {}
    
        let most_times_one_bowler_got_out_one_batsman = {}
    
        for (let index=0; index < delivery_data.length; index++){
    
            let is_dismissed = delivery_data[index].player_dismissed
    
            let dismissal_type = delivery_data[index].dismissal_kind
    
            let batsman= delivery_data[index].batsman
    
            let bowler = delivery_data[index].bowler
    
            if(is_dismissed != ''){
    
                if(dismissal_type !== 'run out'){
        
                    if(!all_bowlers_who_dismissed_a_batsman[batsman]){
        
                        all_bowlers_who_dismissed_a_batsman[batsman] = {}
                    }
        
                    if(!all_bowlers_who_dismissed_a_batsman[batsman][bowler]){
                        all_bowlers_who_dismissed_a_batsman[batsman][bowler] = 0
                    }
        
                    all_bowlers_who_dismissed_a_batsman[batsman][bowler] += 1
        
                }
    
            }
    
        }
    
    
        let maximum = -10000000
    
        for(const batsman in all_bowlers_who_dismissed_a_batsman){
    
            for(const bowlers in all_bowlers_who_dismissed_a_batsman[batsman]){
    
                if(all_bowlers_who_dismissed_a_batsman[batsman][bowlers] > maximum){
    
                    maximum = all_bowlers_who_dismissed_a_batsman[batsman][bowlers]
    
                    most_times_one_bowler_got_out_one_batsman = [ maximum, batsman, bowlers ]
    
                }
            }
    
        }
    
        return most_times_one_bowler_got_out_one_batsman

    }
    catch(err){

        console.log(err.stack)

    }

}

function dump_in_json_file(){

    try{

        const result = highest_number_of_times_one_player_is_dismissed_by_other_player();
        
        let address = 'src/public/output/highestNumberOfTimesOnePlayerIsDismissedByOtherPlayer.json'
        
        fs.outputJson( address, result, {spaces : 2}).
        then(() => fs.readJson(address)).
        then().
        catch(err => {console.log(err)})


    }catch(err){

        console.log(err.stack)

    }

}

dump_in_json_file()