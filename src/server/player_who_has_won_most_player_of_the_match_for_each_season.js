const fs= require('fs-extra')

const {parse} = require('csv-parse/sync')

function player_who_has_won_most_player_of_the_match_for_each_season(){

    try{

        const csv_match_data =  fs.readFileSync('src/data/matches.csv', 'utf-8')
    
        const match_data = parse(
            csv_match_data,
            {
                columns: true,
                skip_empty_lines : true
            }
        )
    
        const csv_delivery_data = fs.readFileSync('src/data/deliveries.csv', 'utf-8')
    
        const delivery_data = parse(
            csv_delivery_data,
            {
                columns: true,
                skip_empty_lines : true
            }
        )
    
    
        let most_player_of_the_match_winners_by_each_season = []
    
        frequency_of_pom_by_each_season = {}
    
        for(let index=0; index< match_data.length; index++){
    
            let season = match_data[index].season
    
            let player_of_match = match_data[index].player_of_match
    
    
            if(!frequency_of_pom_by_each_season[season]){
    
                frequency_of_pom_by_each_season[season] = {}
    
            }
    
            if( !frequency_of_pom_by_each_season[season][player_of_match] ){
    
                frequency_of_pom_by_each_season[season][player_of_match] = 1
    
            }else{
    
                frequency_of_pom_by_each_season[season][player_of_match] += 1
    
            }
    
        }
    
        for( const season in frequency_of_pom_by_each_season){
    
            let maximum= -100000000
    
            let most_player_of_match_winners_for_the_season = []
    
            for( const good_players in frequency_of_pom_by_each_season[season] ){
                
                if(frequency_of_pom_by_each_season[season][good_players] > maximum){
    
                    maximum = frequency_of_pom_by_each_season[season][good_players]
                    
                    most_player_of_match_winners_for_the_season = []

                    most_player_of_match_winners_for_the_season.push( good_players )
    
                }
                else if( frequency_of_pom_by_each_season[season][good_players] == maximum ){

                    most_player_of_match_winners_for_the_season.push( good_players )

                }
            }
    
            most_player_of_the_match_winners_by_each_season.push([ season , most_player_of_match_winners_for_the_season ])
    
        }

        return most_player_of_the_match_winners_by_each_season;
    }
    catch(err){

        throw new Error(err)

    }
    
}

function dump_in_json_file(){

    try{

        const result = player_who_has_won_most_player_of_the_match_for_each_season();
        
        let address = 'src/public/output/playerWhoHasWonMostPlayerOfTheMatchForEachSeason.json'
        
        fs.outputJson( address, result, {spaces : 2}).
        then(() => fs.readJson(address)).
        then().
        catch(err => {console.log(err)})

    }catch(err){
        
        console.log(err.stack)

    }

}

dump_in_json_file()