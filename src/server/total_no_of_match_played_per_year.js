const fs= require('fs-extra')

const {parse} = require('csv-parse/sync')

function total_no_of_match_played_per_year(){

    try{

        const csv_match_data =  fs.readFileSync('src/data/matches.csv', 'utf-8')

        const match_data = parse(
            csv_match_data,
            {
                columns: true,
                skip_empty_lines : true
            }
        )

        const csv_delivery_data = fs.readFileSync('src/data/deliveries.csv', 'utf-8')

        const delivery_data = parse(
            csv_delivery_data,
            {
                columns: true,
                skip_empty_lines : true
            }
        )
    
        let matches_played_per_year = {}
    
        for( let index=0;index< match_data.length; index++ ){
    
            let season = match_data[index].season;
    
            if(!matches_played_per_year[season]){
    
                matches_played_per_year[season] = 0
                
            }
    
            matches_played_per_year[season] += 1
        }

        return matches_played_per_year

    }
    catch(err){

        throw new Error(err)

    }

    
}

function dump_in_json_file(){

    try{

        const result = total_no_of_match_played_per_year();
    
        let address = 'src/public/output/totalNoOfMatchPlayedPerYear.json'
        
        fs.outputJson( address, result, {spaces : 2}).
        then(() => fs.readJson(address)).
        then().
        catch(err => {console.log(err)})

    }
    catch(err){

        console.log(err.stack)

    }
    
}

dump_in_json_file()
