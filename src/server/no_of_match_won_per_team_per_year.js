const fs= require('fs-extra')

const {parse} = require('csv-parse/sync')

function no_of_match_won_per_team_per_year(){

    try{

        const csv_match_data =  fs.readFileSync('src/data/matches.csv', 'utf-8')

        const match_data = parse(
            csv_match_data,
            {
                columns: true,
                skip_empty_lines : true
            }
        )

        const csv_delivery_data = fs.readFileSync('src/data/deliveries.csv', 'utf-8')

        const delivery_data = parse(
            csv_delivery_data,
            {
                columns: true,
                skip_empty_lines : true
            }
        )

    
        let matches_won_by_per_team_per_year = {}
    
        for(let index=0;index<match_data.length; index++){

            let season = match_data[index].season, team1 = match_data[index].team1, team2 = match_data[index].team2 

            let winner = match_data[index].winner;
    
            if(!matches_won_by_per_team_per_year[season]){
                matches_won_by_per_team_per_year[season] = {}
            }
    
            
            if(!matches_won_by_per_team_per_year[season][team1]){
                matches_won_by_per_team_per_year[season][team1] = (winner === team1 ?  1 : 0)
            }else{
                matches_won_by_per_team_per_year[season][team1] += (winner === team1 ? 1 : 0)
            }
    
            if(!matches_won_by_per_team_per_year[season][team2]){
                matches_won_by_per_team_per_year[season][team2] = (winner === team2 ? 1 : 0)
            }else{
                matches_won_by_per_team_per_year[season][team2] += (winner === team2 ? 1 : 0)
            }
            
        }
        
        return matches_won_by_per_team_per_year;
        
    }
    catch(err){

        throw new Error(err)
        
    }

}

function dump_in_json_file(){
    
    try{

        const result = no_of_match_won_per_team_per_year();
    
        let address = 'src/public/output/noOfMatchWonPerTeamPerYear.json'
    
        fs.outputJson( address, result, {spaces : 2}).
        then(() => fs.readJson(address)).
        then().
        catch(err => {console.log(err)})

    }catch(err){

        console.log(err.stack)

    }
}

dump_in_json_file()
