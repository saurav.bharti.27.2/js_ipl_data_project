const fs= require('fs-extra')

const {parse} = require('csv-parse/sync')

function bowler_with_best_economy_in_super_overs(){

    try{

        const csv_match_data =  fs.readFileSync('src/data/matches.csv', 'utf-8')

        const match_data = parse(
            csv_match_data,
            {
                columns: true,
                skip_empty_lines : true
            }
        )

        const csv_delivery_data = fs.readFileSync('src/data/deliveries.csv', 'utf-8')

        const delivery_data = parse(
            csv_delivery_data,
            {
                columns: true,
                skip_empty_lines : true
            }
        )

        let bowler_with_minimum_economy_in_super_overs;

        let bowler_with_super_overs_ball_and_runs = {}

        for(let index =0; index< delivery_data.length; index++){
            
            let bowler = delivery_data[index].bowler

            let super_over = parseInt(delivery_data[index].is_super_over)

            let is_wide = parseInt(delivery_data[index].wide_runs)

            let is_no_ball = parseInt(delivery_data[index].noball_runs)

            let total_runs = parseInt(delivery_data[index].total_runs)

            let is_bye= parseInt(delivery_data[index].bye_runs)
                
            let is_legbye= parseInt(delivery_data[index].legbye_runs)

            if(super_over){

                if(!bowler_with_super_overs_ball_and_runs[bowler]){

                    bowler_with_super_overs_ball_and_runs[bowler] = [0, 0]

                }

                const [balls_bowled_by_him, runs_conceded] = bowler_with_super_overs_ball_and_runs[bowler]

                bowler_with_super_overs_ball_and_runs[bowler] = [ ((is_wide || is_no_ball) ? 0 : 1) + balls_bowled_by_him, runs_conceded + ((is_bye || is_legbye) ? 0 : total_runs) ]

            }
        }

        let minimum = 10000000

        for(const bowler in bowler_with_super_overs_ball_and_runs){

            
            let his_economy = (bowler_with_super_overs_ball_and_runs[bowler][1] / bowler_with_super_overs_ball_and_runs[bowler][0] ) * 6

            if(his_economy < minimum){

                minimum = his_economy;

                bowler_with_minimum_economy_in_super_overs = bowler

            }
        }

        return {bowler_with_minimum_economy_in_super_overs, his_economy : minimum  }

    }
    catch(err){

        throw new Error(err)

    }

}

function dump_in_json_file(){

    try{

        const result = bowler_with_best_economy_in_super_overs();
        
        let address = 'src/public/output/bowlerWithBestEconomyInSuperOvers.json'
        
        fs.outputJson( address, result, {spaces : 2}).
        then(() => fs.readJson(address)).
        then().
        catch(err => {console.log(err)})

    }
    catch(err){

        console.log(err.stack)

    }

}

dump_in_json_file()