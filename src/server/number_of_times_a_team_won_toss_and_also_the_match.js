const fs= require('fs-extra')

const {parse} = require('csv-parse/sync')

function number_of_times_a_team_won_toss_and_also_the_match(){

    try{

        const csv_match_data =  fs.readFileSync('src/data/matches.csv', 'utf-8')

        const match_data = parse(
            csv_match_data,
            {
                columns: true,
                skip_empty_lines : true
            }
        )

        const csv_delivery_data = fs.readFileSync('src/data/deliveries.csv', 'utf-8')

        const delivery_data = parse(
            csv_delivery_data,
            {
                columns: true,
                skip_empty_lines : true
            }
        )

        let total_number_of_times_a_team_won_toss_and_also_the_match = {};

        for(let index=0; index<match_data.length; index++){

            const team_who_won_the_toss = match_data[index].toss_winner

            const team_who_won_the_match = match_data[index].winner

            if(team_who_won_the_match === team_who_won_the_toss){

                if(!total_number_of_times_a_team_won_toss_and_also_the_match[team_who_won_the_toss]){

                    total_number_of_times_a_team_won_toss_and_also_the_match[team_who_won_the_toss] = 0

                }
                total_number_of_times_a_team_won_toss_and_also_the_match[team_who_won_the_toss] += 1

            }

        }

        return total_number_of_times_a_team_won_toss_and_also_the_match

    }catch(err){

        console.log(err.stack)

    }

}

function dump_in_json_file(){

    try{
        
        const result = number_of_times_a_team_won_toss_and_also_the_match();
        
        let address = 'src/public/output/numberOfTimesATeamWonTossAndAlsoTheMatch.json'
        
        fs.outputJson( address, result, {spaces : 2}).
        then(() => fs.readJson(address)).
        then().
        catch(err => {console.log(err)})

    }
    catch(err){

        console.log(err.stack)

    }


}

dump_in_json_file()