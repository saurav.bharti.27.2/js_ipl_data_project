const fs= require('fs-extra')

const { parse } = require('csv-parse/sync')

function top_10_economical_bowlers_in_2015(){

    try{
        
        const csv_match_data =  fs.readFileSync('src/data/matches.csv', 'utf-8')

        const match_data = parse(
            csv_match_data,
            {
                columns: true,
                skip_empty_lines : true
            }
        )

        const csv_delivery_data = fs.readFileSync('src/data/deliveries.csv', 'utf-8')

        const delivery_data = parse(
            csv_delivery_data,
            {
                columns: true,
                skip_empty_lines : true
            }
        )
        
        let best_10_economical_bowlers_in_2015 = []

        let economy_of_bowlers_in_2015 = {}

        for(let index=0; index< delivery_data.length ; index++){

            let match_id = parseInt(delivery_data[index].match_id);
            
            let season = match_data[match_id - 1].season;
            
            if(season === '2015'){

                let bowler = delivery_data[index].bowler
        
                let total_runs_on_a_ball = parseInt(delivery_data[index].total_runs)

                let is_wide = parseInt(delivery_data[index].wide_runs)
                
                let is_no_ball = parseInt(delivery_data[index].noball_runs);

                let is_bye= parseInt(delivery_data[index].bye_runs)
            
                let is_legbye= parseInt(delivery_data[index].legbye_runs)

                if(!economy_of_bowlers_in_2015[bowler]){

                    economy_of_bowlers_in_2015[bowler] = [0, 0]

                }
                let [total_ball_bowled_by_him, runs_conceded] = economy_of_bowlers_in_2015[bowler]

                economy_of_bowlers_in_2015[bowler] = [ total_ball_bowled_by_him + ( (is_wide || is_no_ball) ? 0 : 1) , runs_conceded + ((is_bye || is_legbye ) ? 0: total_runs_on_a_ball) ]

            }

        }

        // Not to count balls - Wide, No ball,

        let bowlers_by_their_economy = []

        for( let bowler in economy_of_bowlers_in_2015){

            bowlers_by_their_economy.push([ (economy_of_bowlers_in_2015[bowler][1]/economy_of_bowlers_in_2015[bowler][0] * 6) , bowler ])
        }

        bowlers_by_their_economy = bowlers_by_their_economy.sort((bowler_a, bowler_b) => (bowler_a[0] - bowler_b[0]))

        for(let index=0; index<10; index++){

            best_10_economical_bowlers_in_2015.push(bowlers_by_their_economy[index])

        }

        return best_10_economical_bowlers_in_2015

    }
    catch(err){

        throw new Error(err)

    }

}

function dump_in_json_file(){
    
    try{
        
        const result = top_10_economical_bowlers_in_2015();
    
        let address = 'src/public/output/top10EconomicalBowlersIn2015.json'
        
        fs.outputJson( address, result, {spaces : 2}).then(() => fs.readJson(address)).then().catch(err => {console.log(err)})

    }catch(err){
        
        console.log(err.stack)

    }

}

dump_in_json_file()