const fs= require('fs-extra')

const {parse} = require('csv-parse/sync')

function strike_rate_of_a_batsman_for_each_season(){

    try{

        const csv_match_data =  fs.readFileSync('src/data/matches.csv', 'utf-8')
    
        const match_data = parse(
            csv_match_data,
            {
                columns: true,
                skip_empty_lines : true
            }
        )
    
        const csv_delivery_data = fs.readFileSync('src/data/deliveries.csv', 'utf-8')
    
        const delivery_data = parse(
            csv_delivery_data,
            {
                columns: true,
                skip_empty_lines : true
            }
        )

        let strike_rate_per_season_for_a_batsman = {}

        let balls_played_vs_scored_for_each_batsman_per_season = {}

        for( let index = 0; index<delivery_data.length; index++){
            
            let match_id = parseInt(delivery_data[index].match_id)

            let season = match_data[match_id - 1].season

            let batsman = delivery_data[index].batsman

            if(!balls_played_vs_scored_for_each_batsman_per_season[batsman]){

                balls_played_vs_scored_for_each_batsman_per_season[batsman] = {}

            }

            if( !balls_played_vs_scored_for_each_batsman_per_season[batsman][season]){

                balls_played_vs_scored_for_each_batsman_per_season[batsman][season] = [0, 0]

            }

            let runs_scored_by_batsman = parseInt(delivery_data[index].batsman_runs)

            let is_wide = parseInt(delivery_data[index].wide_runs)

            let is_no_ball = parseInt(delivery_data[index].noball_runs);

            let [balls_played_by_the_batsman, total_run_of_his] = balls_played_vs_scored_for_each_batsman_per_season[batsman][season]

            balls_played_vs_scored_for_each_batsman_per_season[batsman][season] = [ ((is_wide || is_no_ball) ? 0 : 1) + balls_played_by_the_batsman , runs_scored_by_batsman + total_run_of_his  ];

        }


        for(const batsman in balls_played_vs_scored_for_each_batsman_per_season){

            for(const season_played_by_him in balls_played_vs_scored_for_each_batsman_per_season[batsman]){

                if(!strike_rate_per_season_for_a_batsman[batsman]){

                    strike_rate_per_season_for_a_batsman[batsman] = {}

                }

                if(!strike_rate_per_season_for_a_batsman[batsman][season_played_by_him]){


                    strike_rate_per_season_for_a_batsman[batsman][season_played_by_him] = ( balls_played_vs_scored_for_each_batsman_per_season[batsman][season_played_by_him][1]/balls_played_vs_scored_for_each_batsman_per_season[batsman][season_played_by_him][0] ) * 100 
                }
            }
        }

        return strike_rate_per_season_for_a_batsman

    }
    catch(err){

        throw new Error(err)

    }

}

function dump_in_json_file(){

    try{

        const result = strike_rate_of_a_batsman_for_each_season();
        
        let address = 'src/public/output/strikeRateOfABatsmanForEachSeason.json'
        
        fs.outputJson( address, result, {spaces : 2}).
        then(() => fs.readJson(address)).
        then().
        catch(err => {console.log(err)})

    }catch(err){
        
        console.log(err.stack)

    }

}

dump_in_json_file()
